using NUnit.Framework;
using MyArrayList;
namespace UnitTest_MyArrayList
{
    public class Tests
    {
        myArrayList arrayList;
        int[] _array = { 1, 2, 3, 4, 5, 6, 7, 12, 23, 32, 23, 1, 0, 2 };
        int size = 15;

        [TestCase(new int[14]{ 1, 2, 3, 4, 5, 6, 7, 12, 23, 32, 23, 1, 0, 2 }, 14)]
        [TestCase(new int[1]{1}, 1)]
        public void CheckSizeOfArrayList(int[] array, int excpected)
        {
            arrayList = new myArrayList(array);
            var actual = arrayList.Size();
            Assert.AreEqual(excpected, actual);
        }
        [TestCase(-1, 2)]
        [TestCase(0, 1)]
        [TestCase(13, 2)]
        [TestCase(14, 2)]
        public void CheckGetElementFromArrayList(int index, int excpected)
        {
            arrayList = new myArrayList(_array);
            var actual = arrayList.Get(index);
            Assert.AreEqual(excpected, actual);
        }
        [TestCase(-1, true)]
        [TestCase(0, true)]
        [TestCase(13, true)]
        public void CheckAddedToArrayList(int value, bool excpected)
        {
            arrayList = new myArrayList();
            var actual = arrayList.Add(value);
            Assert.AreEqual(excpected, actual);
        }
        [TestCase(-1, 6, false)]
        [TestCase(0, 7, true)]
        [TestCase(13, 9, true)]
        [TestCase(14, 9, false)]
        public void CheckChangeToArrayList(int index, int value, bool excpected)
        {
            arrayList = new myArrayList(_array);
            var actual = arrayList.Add(index, value);
            Assert.AreEqual(excpected, actual);
        }
        [TestCase(1, 1, new int[] { 2, 3, 4, 5, 6, 7, 1, 0, 23, 2 })]
        [TestCase(2, 2, new int[] { 1, 3, 4, 5, 6, 7, 1, 0, 23, 2 })]
        [TestCase(77, 77, new int[] { 1, 2, 3, 4, 5, 6, 7, 1, 0, 23, 2 })]
        public void CheckRemoveFromArrayList(int value, int excpected, int[] excpArr)
        {
            int[] MyArray = new int[]{ 1, 2, 3, 4, 5, 6, 7, 1, 0, 23, 2 };
            arrayList = new myArrayList(MyArray);
            var actual = arrayList.Remove(value);
            Assert.AreEqual(excpected, actual);
            //CollectionAssert.AreEqual(excpArr, MyArray);  //�������� ����� ��� �� ���... ��� ������ �� �����
        }
        [TestCase(-1, 0, new int[] { 1, 2, 3, 4, 5, 6, 7, 1, 0, 23, 2 })]
        [TestCase(0, 1, new int[] { 2, 3, 4, 5, 6, 7, 1, 0, 23, 2 })]
        [TestCase(10, 2, new int[] { 1, 2, 3, 4, 5, 6, 7, 1, 0, 23 })]
        [TestCase(11, 0, new int[] { 1, 2, 3, 4, 5, 6, 7, 1, 0, 23, 2 })]
        public void CheckRemoveByIndexFromArrayList(int index, int excpectedValue, int[] excpArr)
        {
            int[] MyArray = new int[] { 1, 2, 3, 4, 5, 6, 7, 1, 0, 23, 2 };
            arrayList = new myArrayList(MyArray);
            var actualValue = arrayList.RemoveByIndex(index);
            Assert.AreEqual(excpectedValue, actualValue);
            //CollectionAssert.AreEqual(excpArr, MyArray);  //�������� ����� ��� �� ���... ��� ������ �� �����
        }

        [TestCase(-1, false)]
        [TestCase(0, true)]
        [TestCase(7, true)]
        public void CheckContainsArrayList(int value, bool excpected)
        {
            arrayList = new myArrayList(_array);
            var actual = arrayList.Contains(value);
            Assert.AreEqual(excpected, actual);
        }
        [TestCase(-1, 3, false)]
        [TestCase(0, 2, true)]
        [TestCase(13, 7,true)]
        public void CheckSetArrayList(int index, int value, bool excpected)
        {
            arrayList = new myArrayList(_array);
            var actual = arrayList.Set(index, value);
            Assert.AreEqual(excpected, actual);
        }
        [TestCase(-1, 3, new int[] { 1, 2, 3, 4, 5, 6, 7, 1, 0, 23, 2 })]
        [TestCase(0, 2, new int[] { 2, 2, 3, 4, 5, 6, 7, 1, 0, 23, 2 })]
        [TestCase(10, 7, new int[] { 1, 2, 3, 4, 5, 6, 7, 1, 0, 23, 7 })]
        public void CheckSetArrayList(int index, int value, int[] excpArr)
        {
            int[] MyArray = new int[] { 1, 2, 3, 4, 5, 6, 7, 1, 0, 23, 2 };
            arrayList = new myArrayList(MyArray);
            var actual = arrayList.Set(index, value);
            CollectionAssert.AreEqual(excpArr, MyArray);
        }
        [Test]
        public void CheckToArrayList()
        {
            arrayList = new myArrayList(_array);
            var actual = arrayList.ToArray();
            //Assert.AreEqual(new int[]{ 1, 2, 3, 4, 5, 6, 7, 12, 23, 32, 23, 1, 0, 2 }, actual);  //�������� ����� ��� �� ���... ��� ������ �� �����           
        }
        [TestCase(new int[] { 1, 2, 5, 23, 32 }, true, new int[] { 3, 4, 6, 7, 0 })]
        [TestCase(new int[] { 5, 23, 7 }, true, new int[] { 1, 2, 3, 4, 6, 1, 0, 2 })]
        [TestCase(new int[] { 1, 5, 99, 0 }, true, new int[] { 2, 3, 4, 6, 7, 23, 2 })]
        public void CheckRemoveAllArrayList(int[] arr, bool excpected, int[] excpArr)
        {
            int[] MyArray = new int[] { 1, 2, 3, 4, 5, 6, 7, 1, 0, 23, 2 };
            arrayList = new myArrayList(MyArray);
            var actual = arrayList.RemoveAll(arr);
            Assert.AreEqual(excpected, actual);
           // CollectionAssert.AreEqual(excpArr, MyArray); //�������� ����� ��� �� ���... ��� ������ �� �����
        }
       
    }
}